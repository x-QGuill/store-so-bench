# store-so-bench

Simple application to study the loading of SO libraries in a store-based file system.

## How to run?

Everything is done via `Snakemake`.

```sh
nix develop --command snakemake -c 4
```

It will create the binaries with the desired number of SO libraries and run them with `strace`.

Once the binaries ran, it will generate the plots in the `figs` folder.


