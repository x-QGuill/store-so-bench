import os
store = config["store"]
here = os.getcwd()

rule generate_c_lib:
  input:
    "templates/template.c"
  output:
    f"{here}/{store}/hash-lib{{id}}/lib/lib{{id}}.so"
  shell:
    "gcc -shared -o {output} -Wl,-soname,lib{wildcards.id}.so -x c {input}"
