import os
store = config["store"]
here = os.getcwd()
# nb_libs = config["nb_libs"]

def lib_selection(wildcards):
  return expand(f"{here}/{store}/hash-lib{{id}}/lib/lib{{id}}.so", id=range(int(wildcards.nb_libs)))

def get_cpaths(wildcards):
  return " ".join(f"-L{here}/{store}/hash-lib{lib_id}/lib/" for lib_id in range(int(wildcards.nb_libs)))

def get_rpaths(wildcards):
  return ":".join(f"{here}/{store}/hash-lib{lib_id}/lib" for lib_id in range(int(wildcards.nb_libs)))

def get_cflags(wildcards):
  return " ".join(expand("-l{lib_id}", lib_id=range(int(wildcards.nb_libs))))



rule generate_app:
  input:
    main = "templates/main.c",
    libs = lib_selection,
    # libs = expand(f"{here}/{store}/hash-lib{{id}}/lib/lib{{id}}.so", id=range(nb_libs))
  params:
    cpaths = get_cpaths,
    rpaths = get_rpaths,
    cflags = get_cflags
    # cpaths = " ".join(f"-L{here}/{store}/hash-lib{lib_id}/lib/" for lib_id in range(wildcards.nb_libs)),
    # rpaths = ":".join(f"{here}/{store}/hash-lib{lib_id}/lib" for lib_id in range(nb_libs)),
    # cflags = " ".join(expand("-l{lib_id}", lib_id=range(nb_libs)))
  output:
    f"{here}/{store}/hash-app_{{nb_libs}}/bin/app_{{nb_libs}}"
  log: "logs/generate_app_{nb_libs}"
  shell:
    "echo {params.rpaths} > {log} ;mpicc -o {output} {params.cpaths} {params.cflags} -Wl,-rpath,{params.rpaths} -x c {input.main}"
  
rule link_app:
  input:
    f"{here}/{store}/hash-app_{{nb_libs}}/bin/app_{{nb_libs}}"
  output:
    "app_{nb_libs}"
  shell:
    "ln -s {input} {output}"
    
