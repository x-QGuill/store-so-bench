store = config["store"]
here = os.getcwd()

rule run_strace:
  input:
    f"{here}/{store}/hash-{{app}}/bin/{{app}}"
  output:
    "data/strace/{app}/log.raw"
  shell:
    "strace -c -o {output} {input}"
  
rule clean_strace_log:
  input:
    "data/strace/{app}/log.raw"
  output:
    "data/strace/{app}/log.clean"
  shell:
    "cat {input} | tail -n +3 | head -n -2 | awk -v FIELDWIDTHS='6 12 12 10 10 25' '{{print $1 \",\" $2 \",\" $3 \",\" $4 \",\" $5 \",\" $6;}}' > {output}"

rule generate_strace_csv_app:
  input:
    "workflow/scripts/R/read_strace.R",
    "data/strace/app_{nb_lib}/log.clean"
  output:
    "data/strace/app_{nb_lib}/log.csv"
  shell:
    "nix develop .#rshell --command Rscript {input} {wildcards.nb_lib} {output}"
  
rule plot_app:
  input:
    "workflow/scripts/R/plot_graph.R",
    expand("data/strace/app_{nb_lib}/log.csv", nb_lib=config["nb_libs"])
  output:
    "figs/graph.pdf"
  shell:
    "nix develop .#rshell --command Rscript {input} {output}"
  
  

