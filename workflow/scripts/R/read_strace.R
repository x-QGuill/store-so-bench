library(tidyverse)

args = commandArgs(trailingOnly=TRUE)
nb_args = length(args)
name = args[1]
nb_lib = args[2]
outname = args[3]

read_csv(name, col_types = "ddnnnc", trim_ws = TRUE, col_names = c("time", "sec", "usec_per_call", "calls", "errors", "syscall")) %>%
      mutate(errors = if_else(is.na(errors), 0, errors)) %>%
      mutate(nb_lib = nb_lib) %>%
      write_csv(x = ., file = outname, col_names = TRUE)
